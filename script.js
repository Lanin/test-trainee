/**
 * Created by Evgeni on 14.11.2016.
 */
$(document).ready(function () {

    $.mask.definitions['~'] = '[+-]';
    $("#phone").mask("380(99)999-99-99");

    $('input#name, input#email, input#phone').unbind().blur(function () {

        var id = $(this).attr('id');
        var val = $(this).val();

        switch (id) {
            case 'name':
                var rv_name = /^[a-zA-Zа-яА-Я]+$/;

                if (val.length >= 6 && val != '' && rv_name.test(val) && val.length <= 12) {
                    $(this).removeClass('error').addClass('not_error');
                    $(this).next('.error-box').text('Принято')
                        .css('color', 'green')
                        .animate({'paddingLeft': '10px'}, 400)
                        .animate({'paddingLeft': '5px'}, 400);
                }
                else {
                    $(this).removeClass('not_error').addClass('error');
                    $(this).next('.error-box').html('поле "Имя" обязательно для заполнения<br>, длина имени должна составлять не менее 6 символов и не более 12<br>, поле должно содержать только русские или латинские буквы')
                        .css('color', 'red')
                        .animate({'paddingLeft': '10px'}, 400)
                        .animate({'paddingLeft': '5px'}, 400);
                }
                break;

            case 'email':
                var rv_email = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
                if (val != '' && rv_email.test(val)) {
                    $(this).removeClass('error').addClass('not_error');
                    $(this).next('.error-box').text('Принято')
                        .css('color', 'green')
                        .animate({'paddingLeft': '10px'}, 400)
                        .animate({'paddingLeft': '5px'}, 400);
                }
                else {
                    $(this).removeClass('not_error').addClass('error');
                    $(this).next('.error-box').html('поле "Email" обязательно для заполнения<br>, поле должно содержать правильный email-адрес<br>')
                        .css('color', 'red')
                        .animate({'paddingLeft': '10px'}, 400)
                        .animate({'paddingLeft': '5px'}, 400);
                }
                break;

            case 'phone':
                if (val != '') {
                    $(this).removeClass('error').addClass('not_error');
                    $(this).next('.error-box').text('Принято')
                        .css('color', 'green')
                        .animate({'paddingLeft': '10px'}, 400)
                        .animate({'paddingLeft': '5px'}, 400);
                }
                else {
                    $(this).removeClass('not_error').addClass('error');
                    $(this).next('.error-box').html('поле "Телефон" обязательно для заполнения')
                        .css('color', 'red')
                        .animate({'paddingLeft': '10px'}, 400)
                        .animate({'paddingLeft': '5px'}, 400);
                }
                break;
        }
    });

    $('form').submit(function (e) {

        e.preventDefault();

        if ($('.not_error').length == 3) {
            $.ajax({
                url: 'form.php',
                type: 'post',
                data: $(this).serialize(),
                success: function (data) {
                    $('#result').text("Данные успешно отправленны");
                    $('form').trigger('reset');
                    $('.error-box').text('');
                },
                error: function (xhr, str) {
                    alert('Возникла ошибка: ' + xhr.responseCode);
                }
            });
        }
        else {
            alert('Возникла ошибка');
            return false;
        }
    });
});